<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%report}}', [
            'id' => $this->primaryKey(),
            'account' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'currency' => $this->string()->notNull(),
            'leverage' => $this->string()->notNull(),
            'datetime' => $this->string()->notNull(),

        ], $tableOptions);

        $this->createTable('{{%transaction}}', [
            'ticket' => $this->primaryKey(),
            'report_id' => $this->integer()->notNull(),
            'type' => "ENUM('balance', 'buy', 'buy stop') NOT NULL",
            'open_time' => $this->string()->notNull(),
            'close_time' => $this->string()->defaultValue(null),
            'profit' => $this->decimal(10, 2)->defaultValue(0),
            'info' => $this->string()->defaultValue(null),

            'size' => $this->string()->defaultValue(null),
            'item' => $this->string()->defaultValue(null),
            's_l' => $this->string()->defaultValue(null),
            't_p' => $this->string()->defaultValue(null),
            'price' => $this->string()->defaultValue(null),
            'commission' => $this->string()->defaultValue(null),
            'taxes' => $this->string()->defaultValue(null),
            'swap' => $this->string()->defaultValue(null),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-report-transaction',
            '{{%transaction}}',
            'report_id',
            '{{%report}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropTable('{{%report}}');
        $this->dropTable('{{%transaction}}');
    }
}
