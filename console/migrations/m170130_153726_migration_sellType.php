<?php

use yii\db\Migration;

class m170130_153726_migration_sellType extends Migration
{
    public function up()
    {
        $this->alterColumn('transaction', 'type', "ENUM('balance', 'buy', 'buy stop', 'sell') NOT NULL");
    }

    public function down()
    {
        $this->alterColumn('transaction', 'type', "ENUM('balance', 'buy', 'buy stop') NOT NULL");
    }
}
