<?php

use yii\db\Migration;

class m170130_152531_reportLeverage_optional extends Migration
{
    public function up()
    {
        $this->alterColumn('report', 'leverage', $this->string()->defaultValue(null));
    }

    public function down()
    {
        $this->alterColumn('report', 'leverage', $this->string()->notNull());

    }
}
