<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "report".
 *
 * @property integer $id
 * @property integer $account
 * @property string $name
 * @property string $currency
 * @property string $leverage
 * @property string $datetime
 */
class Report extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account'], 'integer'],
            [['name', 'currency', 'datetime'], 'required'],
            [['name', 'currency', 'leverage', 'datetime'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account' => 'Account',
            'name' => 'Name',
            'currency' => 'Currency',
            'leverage' => 'Leverage',
            'datetime' => 'Datetime',
        ];
    }

    public function loadData($file)
    {
        $DOM = new \DOMDocument();
        if ($DOM->loadHTMLFile($file)) {
            $trItems = $DOM->getElementsByTagName('tr');

            // php7
            if ($trItems->item(0) === null) {
//            if (!isset($trItems[0])) {
                $this->addError('id', 'Invalid html report file');
                return false;
            }

            // php7
            $tdItems = $trItems->item(0)->childNodes;
//            $tdItems = $trItems[0]->childNodes;

            foreach ($tdItems as $tdItem) {
                if (preg_match('/(?P<attribute>\S+): (?P<value>.+)/', $tdItem->textContent, $matches)) {
                    $attribute = strtolower($matches['attribute']);
                    $value = $matches['value'];
                    $this->$attribute = $value;
                }

                if (preg_match('/(\d+) (\S+) .+/', $tdItem->textContent)) {
                    $this->datetime = $tdItem->textContent;
                }
            }

            $report = Report::findOne([
                'account' => $this->account,
                'datetime' => $this->datetime,
            ]);
            if ($report) {
                $this->addError('id', 'Dublicate report');
                return false;
            }

            return true;
        } else {
            $this->addError('id', 'Invalid html report file');
            return false;
        }
    }
}
