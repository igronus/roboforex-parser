<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property integer $ticket
 * @property string $type
 * @property string $open_time
 * @property string $close_time
 * @property string $profit
 * @property string $info
 * @property string $size
 * @property string $item
 * @property string $s_l
 * @property string $t_p
 * @property string $price
 * @property string $commission
 * @property string $taxes
 * @property string $swap
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'open_time'], 'required'],
            [['type'], 'string'],
            [['report_id', 'profit'], 'number'],
            [['open_time', 'close_time', 'info', 'size', 'item', 's_l', 't_p', 'price', 'commission', 'taxes', 'swap'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ticket' => 'Ticket',
            'report_id' => 'Report ID',
            'type' => 'Type',
            'open_time' => 'Open Time',
            'close_time' => 'Close Time',
            'profit' => 'Profit',
            'info' => 'Info',
            'size' => 'Size',
            'item' => 'Item',
            's_l' => 'S L',
            't_p' => 'T P',
            'price' => 'Price',
            'commission' => 'Commission',
            'taxes' => 'Taxes',
            'swap' => 'Swap',
        ];
    }

    public static function loadData($file, $report_id)
    {
        $DOM = new \DOMDocument();
        $DOM->loadHTMLFile($file);

        $items = $DOM->getElementsByTagName('tr');

        foreach ($items as $item) {
            $start = $end = false;

            if ($item->hasAttributes()) {
                foreach ($item->attributes as $attr) {
                    $name = $attr->nodeName;
                    $value = $attr->nodeValue;
                    if ($name == 'align' && $value == 'right') {
                        $start = true;
                    }
                }
            }

            if (!$start) {
                continue;
            }

            $transaction = new Transaction();
            $transaction->report_id = $report_id;

            foreach ($item->childNodes as $k => $childNode) {
                switch ($k) {
                    case 0:
                        $transaction->ticket = $childNode->nodeValue;
                        break;
                    case 1:
                        $transaction->open_time = $childNode->nodeValue;
                        break;
                    case 2:
                        $transaction->type = $childNode->nodeValue;
                        break;
                    case 3:
                        switch ($transaction->type) {
                            case 'buy':
                            case 'buy stop':
                            case 'sell':
                                $transaction->size = $childNode->nodeValue;
                                break;
                            case 'balance':$transaction->profit;
                                $transaction->info = $childNode->nodeValue;
                                break;
                            default:
                                $end = true;
                                break;
                        }
                        break;
                    case 4:
                        switch ($transaction->type) {
                            case 'buy':
                            case 'buy stop':
                            case 'sell':
                                $transaction->item = $childNode->nodeValue;
                                break;
                            case 'balance':$transaction->profit;
//                                $transaction->profit = $childNode->nodeValue;
                                $profit = str_replace(' ', '', $childNode->nodeValue);
                                $transaction->profit = $profit;
                                break;
                            default:
                                $end = true;
                                break;
                        }
                        break;
                    case 5:
                        $transaction->price = $childNode->nodeValue;
                        break;
                    case 6:
                        $transaction->s_l = $childNode->nodeValue;
                        break;
                    case 7:
                        $transaction->t_p = $childNode->nodeValue;
                        break;
                    case 8:
                        $transaction->close_time = $childNode->nodeValue;
                        break;
                    case 9:
                        $transaction->price = $childNode->nodeValue;
                        break;
                    case 10:
                        switch ($transaction->type) {
                            case 'buy':
                            case 'sell':
                                $transaction->commission = $childNode->nodeValue;
                                break;
                            case 'buy_stop':
                                $transaction->info = $childNode->nodeValue;
                                break;
                        }
                        break;
                    case 11:
                        $transaction->taxes = $childNode->nodeValue;
                        break;
                    case 12:
                        $transaction->swap = $childNode->nodeValue;
                        break;
                    case 13:
//                        $transaction->profit = $childNode->nodeValue;
                        $profit = str_replace(' ', '', $childNode->nodeValue);
                        $transaction->profit = $profit;
                        break;
                }

            }

            if ($end) {
                break;
            }

            $oldTransaction = Transaction::findOne($transaction->ticket);
            if ($oldTransaction) {
                return false;
            }

            if (!$transaction->insert()) {
                return false;
            }
        }

        return true;
    }
}
