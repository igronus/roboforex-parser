<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ReportForm extends Model
{
    public $fileReport;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['fileReport'], 'required'],
//            ['fileReport', 'file'],
            ['fileReport', 'file', 'extensions' => 'html'],
        ];
    }
}
