<?php
namespace frontend\controllers;

use common\models\Report;
use common\models\Transaction;

use frontend\models\ReportForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays list of imported reports.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ReportForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $reportFile = UploadedFile::getInstance($model, 'fileReport');

            $transaction = \Yii::$app->db->beginTransaction();
            $report = new Report();
            if ($report->loadData($reportFile->tempName) && $report->save()) {
                if (Transaction::loadData($reportFile->tempName, $report->id)) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'File uploaded');
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', 'Transactions error');
                }
            } else {
                $transaction->rollBack();
                foreach ($report->errors as $error) {
                    Yii::$app->session->setFlash('error', $error);
                }
            }
        }

        $reports = new ActiveDataProvider([
            'query' => Report::find()
        ]);

        return $this->render('index', [
            'reports' => $reports,
            'model' => $model,
        ]);
    }

    /**
     * Displays report.
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $report = $this->findReport($id);
        $transactionQuery = Transaction::find()->where(['report_id' => $id]);
        $transactionsProvider = new ActiveDataProvider([
            'query' => $transactionQuery,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $chartDatasetBalance['label'] = 'balance';
        $chartDatasetProfit['label'] = 'profit';
        $chartDatasetProfit['backgroundColor'] = 'red';

        $transactions = $transactionQuery->all();
        $balance = 0; $chartLabels = [];
        foreach ($transactions as $transaction) {
            $balance += $transaction->profit;
            $chartDatasetBalance['data'][] = $balance;
            $chartDatasetProfit['data'][] = $transaction->profit;
            $chartLabels[] = $transaction->open_time;
        }

        return $this->render('view', [
            'report' => $report,
            'transactionsProvider' => $transactionsProvider,
            'chartDatasetProfit' => $chartDatasetProfit,
            'chartDatasetBalance' => $chartDatasetBalance,
            'chartLabels' => $chartLabels,
        ]);
    }

    /**
     * Displays transaction.
     *
     * @return mixed
     */
    public function actionViewTransaction($id)
    {
        $transaction = $this->findTransaction($id);
        $report = $this->findReport($transaction->report_id);

        return $this->render('view-transaction', [
            'transaction' => $transaction,
            'report' => $report,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Finds the Report model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Report the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findReport($id)
    {
        if (($model = Report::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Transaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findTransaction($id)
    {
        if (($model = Transaction::findOne(['ticket' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
