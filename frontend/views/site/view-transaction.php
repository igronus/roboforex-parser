<?php

/* @var $this yii\web\View */
/* @var $transaction common\models\Transaction */
/* @var $report common\models\Report */

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Transaction ' . $transaction->ticket;
$this->params['breadcrumbs'][] = ['label' => $report->name . ' :: ' . $report->datetime,
    'url' => ['view', 'id' => $report->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $transaction,
    ]) ?>

</div>
