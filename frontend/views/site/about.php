<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>This application is used to parse and analyse HTML-reports generated by Roboforex.</p>

    <p>Source code is available at bitbucket:<br><code>https://bitbucket.org/igronus/roboforex-parser</code></p>

    <p>Live demo is available at address:<br><code>http://roboforex.pellinen.tk/</code></p>

    <p>Based on Yii 2 Advanced Project Template.</p>
</div>
