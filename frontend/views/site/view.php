<?php

/* @var $this yii\web\View */
/* @var $report common\models\Report */
/* @var $transactionsProvider yii\data\ActiveDataProvider */
/* @var $chartDatasetBalance array */
/* @var $chartDatasetProfit array */
/* @var $chartLabels array */

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\chartjs\ChartJs;
use yii\grid\GridView;

$this->title = $report->name . ' :: ' . $report->datetime;
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= ChartJs::widget([
        'type' => 'line',
        'options' => [
            'height' => 200,
            'width' => 600,

        ],
        'clientOptions' => [
            'tooltips' => [
//                'enabled' => false,
//                'callbacks' => [
//                    'label' => 'function(tooltipItem, data) {
//                          return 2 };'
//                ]
//                'tooltipTemplate' => "234"
//                'custom' => 'function(tooltip) {
//                     tooltip will be false if tooltip is not visible or should be hidden
//                    if (!tooltip) {
//                        return;
//                    }
//                }'
            ]
        ],
        'data' => [
            'labels' => $chartLabels,
            'datasets' => [ $chartDatasetBalance, $chartDatasetProfit ],
        ]
    ]);?>

    <h2>Report information</h2>

    <?= DetailView::widget([
        'model' => $report,
        'attributes' => [
            'name',
            'account',
            'currency',
            'leverage',
            'datetime',
        ]
    ]) ?>

    <h2>Transactions</h2>

    <?php \yii\widgets\Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $transactionsProvider,
        'columns' => [
            'ticket',
            'type',
            'open_time',
            'profit',
            'info',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view-transaction}',
                'buttons' => [
                    'view-transaction' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>',
                            $url,[$model->ticket]);
                    }
                ],
            ],
        ]
    ]); ?>

    <?php \yii\widgets\Pjax::end(); ?>

</div>
