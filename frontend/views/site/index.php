<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $reports yii\data\ActiveDataProvider */
/* @var $model frontend\models\ReportForm */

$this->title = 'Roboforex parser';
?>
<div class="site-index">

    <h1>Available reports</h1>

    <?= GridView::widget([
        'dataProvider' => $reports,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
            ],

            'account',
            'name',
            'datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
